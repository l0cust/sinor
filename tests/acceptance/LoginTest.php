<?php
// Below the admin user is used as an example, but it works for any users.

use PHPUnit\Framework\TestCase;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy as by;
use Facebook\WebDriver\WebDriverExpectedCondition as condition;

class LoginTest extends TestCase{
 private $driver;
 
 protected function setUp(){
  $this->driver=RemoteWebDriver::create("http://localhost:4444/wd/hub",DesiredCapabilities::chrome());
  $this->driver->get("http://localhost:8000");
 }
 public function testCorrectLogIn(){
  $this->assertEquals($this->driver->findElement(by::tagName("h1"))->getText(),"Logowanie");
  $this->driver->findElement(by::id("login_username"))->sendKeys("admin");
  $this->driver->findElement(by::id("login_password"))->sendKeys("nocne@radio!001");
  $this->driver->findElement(by::id("login_signin"))->click();
  $this->driver->wait(3,500)->until(condition::presenceOfElementLocated(by::id("navigation_loginInfo")));
  $this->assertEquals($this->driver->getTitle(),"Nocne Radio");
  $this->driver->findElement(by::id("navigation_logout"))->click();
  $this->assertTrue(strpos($this->driver->getTitle(),"Logowanie")!==false);
 }
 public function testIncorrectLogIn_passwordTooShort(){
  $this->driver->findElement(by::id("login_username"))->sendKeys("admin");
  $this->driver->findElement(by::id("login_password"))->sendKeys("pass");
  $this->driver->findElement(by::id("login_signin"))->click();
  $this->driver->wait(3,500)->until(condition::presenceOfElementLocated(by::className("errorMessage")));
  $this->assertTrue(strpos($this->driver->findElement(by::className("errorMessage"))->getText(),"Hasło jest zbyt krótkie.")!==false);
 }
 public function testIncorrectLogIn_wrongUsername(){
  $this->driver->findElement(by::id("login_username"))->sendKeys("usr@%");
  $this->driver->findElement(by::id("login_password"))->sendKeys("nocne@radio!001");
  $this->driver->findElement(by::id("login_signin"))->click();
  $this->driver->wait(3,500)->until(condition::presenceOfElementLocated(by::className("errorMessage")));
  $this->assertTrue(strpos($this->driver->findElement(by::className("errorMessage"))->getText(),"Nieprawidłowa nazwa użytkownika.")!==false);
 }
 public function testIncorrectLogIn_badCredentials(){
  $this->driver->findElement(by::id("login_username"))->sendKeys("username");
  $this->driver->findElement(by::id("login_password"))->sendKeys("passphrase");
  $this->driver->findElement(by::id("login_signin"))->click();
  $this->driver->wait(3,500)->until(condition::presenceOfElementLocated(by::className("errorMessage")));
  $this->assertTrue(strpos($this->driver->findElement(by::className("errorMessage"))->getText(),"Nieprawidłowe dane logowania.")!==false);
 }
 protected function tearDown(){
  $this->driver->quit();
 }
}
