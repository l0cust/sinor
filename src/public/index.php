<?php

use Phalcon\Loader;
use Phalcon\Mvc\Application;

try{
 $config=include_once("../common/config/config.php");
 $loader=new Loader();
 $loader->registerNamespaces([
  "Sinor\Controller"=>$config->directories->controllers,
  "Sinor\Form"=>$config->directories->forms,
  "Sinor\Model"=>$config->directories->models,
  "Sinor\Validator"=>$config->directories->validators
 ])->register();
 include_once("../common/config/services.php");
 $application=new Application($di);
 echo $application->handle()->getContent();
}
catch(Exception $ex){
 echo "<pre>";
 var_dump($ex);
 echo "</pre>";
}
