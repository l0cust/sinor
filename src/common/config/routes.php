<?php

use Phalcon\Mvc\Router;

$router=new Router();
$router->add("/",["controller"=>"Sinor\Controller\Index","action"=>"index"])->setName("sinor_index");
$router->add("/login",["controller"=>"Sinor\Controller\Index","action"=>"login"])->setName("sinor_login");
$router->add("/logout",["controller"=>"Sinor\Controller\Index","action"=>"logout"])->setName("sinor_logout");
$router->add("/users",["controller"=>"Sinor\Controller\Users","action"=>"index"])->setName("sinor_users");
$router->add("/users/new",["controller"=>"Sinor\Controller\Users","action"=>"newUser"])->setName("sinor_newUser");
$router->add("/users/{user_id:\d+}/edit",["controller"=>"Sinor\Controller\Users","action"=>"editUser"])->setName("sinor_editUser");
$router->add("/users/{user_id:\d+}/delete",["controller"=>"Sinor\Controller\Users","action"=>"deleteUser"])->setName("sinor_deleteUser");
$router->addPost("/users/delete","Sinor\Controller\Users::delete")->setName("sinor_deleteUsers");
$router->add("/sponsors",["controller"=>"Sinor\Controller\Sponsors","action"=>"index"])->setName("sinor_sponsors");
$router->add("/sponsors/page/{page_number:\d+}",["controller"=>"Sinor\Controller\Sponsors","action"=>"index"])->setName("sinor_sponsorsPagination");
$router->add("/sponsors/sort/{sort_type:name|surname|nickname}",["controller"=>"Sinor\Controller\Sponsors","action"=>"sort"])->setName("sinor_sponsorsSorting");
$router->addPost("/sponsors/search",["controller"=>"Sinor\Controller\Sponsors","action"=>"search"])->setName("sinor_searchSponsor");
$router->add("/sponsors/new",["controller"=>"Sinor\Controller\Sponsors","action"=>"newSponsor"])->setName("sinor_newSponsor");
$router->add("/sponsors/{sponsor_id:\d+}",["controller"=>"Sinor\Controller\Sponsors","action"=>"show"])->setName("sinor_viewSponsor");
$router->add("/sponsors/{sponsor_id:\d+}/edit",["controller"=>"Sinor\Controller\Sponsors","action"=>"editSponsor"])->setName("sinor_editSponsor");
$router->add("/sponsors/{sponsor_id:\d+}/delete",["controller"=>"Sinor\Controller\Sponsors","action"=>"deleteSponsor"])->setName("sinor_deleteSponsor");
$router->addPost("/sponsors/delete",["controller"=>"Sinor\Controller\Sponsors","action"=>"delete"])->setName("sinor_deleteSponsors");
$router->add("/donates",["controller"=>"Sinor\Controller\Donates","action"=>"index"])->setName("sinor_donates");
$router->add("/donates/new",["controller"=>"Sinor\Controller\Donates","action"=>"newDonate"])->setName("sinor_newDonate");
$router->add("/donates/{donate_id:\d+}/edit",["controller"=>"Sinor\Controller\Donates","action"=>"editDonate"])->setName("sinor_editDonate");
$router->add("/donates/{donate_id:\d+}/delete",["controller"=>"Sinor\Controller\Donates","action"=>"deleteDonate"])->setName("sinor_deleteDonate");
$router->addPost("/donates/delete",["controller"=>"Sinor\Controller\Donates","action"=>"delete"])->setName("sinor_deleteDonates");
return $router;
