<?php

use Phalcon\Config;

return new Config([
 "application"=>[
  "name"=>"sinor",
  "base_uri"=>"/",
 ],
 "directories"=>[
  "controllers"=>"../app/controllers",
  "forms"=>"../app/forms",
  "models"=>"../app/models",
  "validators"=>"../app/validators",
  "views"=>"../app/views"
 ],
 "database"=>[
  "host"=>"localhost",
  "port"=>3306,
  "username"=>"root",
  "password"=>"kunacobar0sc0mysql",
  "dbname"=>"sinor"
 ],
]);