<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Url as BaseUrl;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\PDO\Mysql as DBMysql;
use Phalcon\Session\Adapter\Files as SessionFiles;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Translate\Adapter\NativeArray;

$di=new FactoryDefault();
$di->set("url", function() use ($config){
 $url=new BaseUrl();
 $url->setBaseUri($config->application->base_uri);
 return $url;
},true);
$di->set("router", function(){
 return include_once("routes.php");
},true);
$di->set("view", function() use ($config){
 $view=new View();
 $view->setViewsDir($config->directories->views);
 return $view;
},true);
$di->set("db", function() use ($config){
 return new DBMysql([
  "host"=>$config->database->host,
  "port"=>$config->database->port,
  "username"=>$config->database->username,
  "password"=>$config->database->password,
  "dbname"=>$config->database->dbname
 ]);
},true);
$di->set("session", function() use ($config){
 $session=new SessionFiles(["uniqueId"=>$config->application->name]);
 $session->start();
 return $session;
},true);
$di->set("flashSession", function(){
 return new FlashSession();
},true);
$di->set("translate", function() use ($di){
 $lang=$di["request"]->getBestLanguage();
 if(file_exists("../common/translation/".$lang.".php")) include_once("../common/translation/".$lang.".php");
 else include_once("../common/translation/pl.php");
 return new NativeArray(["content"=>$messages]);
},true);
$di->set("config",$config);
