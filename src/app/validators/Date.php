<?php

namespace Sinor\Validator;

use Phalcon\Mvc\EntityInterface;
use Phalcon\Mvc\Model\Validator;
use Phalcon\Mvc\Model\ValidatorInterface;

class Date extends Validator implements ValidatorInterface{
 protected $field;
 protected $values;

 public function validateYear(){
  if(strlen($this->values[0])!==4){
   $this->appendMessage("Liczba oznaczająca rok powinna być czterocyfrowa.",$this->field,"Date");
   return false;
  }
  if(intval($this->values[0])<=0){
   $this->appendMessage("Rok powinien być liczbą naturalną.",$this->field,"Date");
   return false;
  }
  if(intval($this->values[0])<2015){
   $this->appendMessage("Rok musi być większy od 2014, ponieważ w roku 2015 powstało Nocne Radio.",$this->field,"Date");
   return false;
  }
  return true;
 }
 public function validateMonth(){
  if(strlen($this->values[1])!==2){
   $this->appendMessage("Liczba oznaczająca miesiąc powinna być dwucyfrowa.",$this->field,"Date");
   return false;
  }
  if(intval($this->values[1])<1 || intval($this->values[1])>12){
   $this->appendMessage("Liczba oznaczająca miesiąc musi być w przedziale 1-12 (01-12).",$this->field,"Date");
   return false;
  }
  return true;
 }
 public function validateDay(){
  if(strlen($this->values[2])!==2){
   $this->appendMessage("Liczba oznaczająca dzień powinna być dwucyfrowa.",$this->field,"Date");
   return false;
  }
  if(intval($this->values[2])<1 || intval($this->values[2])>31){
   $this->appendMessage("Liczba oznaczająca dzień musi być w przedziale 1-31 (01-31)",$this->field,"Date");
   return false;
  }
  return true;
 }
 public function validate(EntityInterface $model){
  $this->field=$this->getOption("field");
  $fld=$this->field;
  $this->values=explode("-",$model->$fld);
  if(count($this->values)!==3){
   $this->appendMessage("Pole ".$this->field." nie jest datą lub podane zostało w nieprawidłowym formacie (yyyy-mm-dd).",$this->field,"Date");
   return false;
  }
  return ($this->validateYear() && $this->validateMonth() && $this->validateDay());
 }
}