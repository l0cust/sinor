<?php

namespace Sinor\Validator;

use Phalcon\Mvc\EntityInterface;
use Phalcon\Mvc\Model\Validator;
use Phalcon\Mvc\Model\ValidatorInterface;

class Time extends Validator implements ValidatorInterface{
 protected $field;
 protected $values;

 public function validateHour(){
  if(strlen($this->values[0])!==2){
   $this->appendMessage("Liczba oznaczająca godzinę powinna być dwucyfrowa.",$this->field,"Time");
   return false;
  }
  if(intval($this->values[0])<0 || intval($this->values[0])>23){
   $this->appendMessage("Liczba oznaczająca godzinę powinna znajdować się w przedziale 0-23 (00-23).",$this->field,"Time");
   return false;
  }
  return true;
 }
 public function validateMinutes(){
  if(strlen($this->values[1])!==2){
   $this->appendMessage("Liczba oznaczająca minuty powinna być dwucyfrowa.",$this->field,"Time");
   return false;
  }
  if(intval($this->values[1])<0 || intval($this->values[1])>59){
   $this->appendMessage("Liczba oznaczająca minuty powinna znajdować się w przedziale 0-59 (00-59)",$this->field,"Time");
   return false;
  }
  return true;
 }
 public function validate(EntityInterface $model){
  $this->field=$this->getOption("field");
  $fld=$this->field;
  $this->values=explode(":",$model->$fld);
  if(count($this->values)===0){
   $this->appendMessage("Pole ".$this->field." nie jest czasem lub podane zostało w nieprawidłowym formacie (hh:mm)",$this->field,"Time");
   return false;
  }
  return ($this->validateHour() && $this->validateMinutes());
 }
}