<?php

namespace Sinor\Controller;

use Phalcon\Mvc\Controller;
use Phalcon\Tag;

class BaseController extends Controller{
 public function initialize(){
  $this->tag->setDoctype(Tag::XHTML5);
  $this->tag->setTitle($this->translate->_("title"));
  $this->assets->addJs("https://code.jquery.com/jquery-1.12.3.min.js",false)
               ->addJs("js/bindings.js")
               ->addJs("js/form_events.js");
 }
 public function isLogged(){
  if(!$this->session->has("access")){
   $this->session->set("destination",$this->request->getURI());
   $this->response->redirect($this->url->get(["for"=>"sinor_login"]));
  }
  else return true;
 }
}
