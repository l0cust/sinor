<?php

namespace Sinor\Controller;

use Sinor\Form\Donate;
use Sinor\Model\Donates;
use Sinor\Model\Sponsors;

class DonatesController extends BaseController{
 public function indexAction(){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("navigation_donates"));
   $this->view->donates=Donates::find();
   $this->view->donates_amount=Donates::count();
  }
 }
 public function newDonateAction(){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("donate_addition"));
   $form=new Donate();
   if(!$this->request->getPost()) $this->view->form=$form;
   elseif($this->request->getPost()){
    $nd=new Donates();
    if($form->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
     if($nd->save($this->request->getPost())){
      $this->flashSession->success($this->translate->_("donate_addition_success"));
      $this->response->redirect($this->url->get(["for"=>"sinor_donates"]));
     }
     else{
      foreach($nd->getMessages() as $message) $this->flashSession->error($message->getMessage());
      $this->view->form=$form;
     }
    }
    else{
     foreach($form->getMessages() as $message) $this->flashSession->error($message->getMessage());
     if(!count($form->getMessages()) && !$this->session->has("future_dates_validation")) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
     $this->view->form=$form;
    }
   }
  }
 }
 public function editDonateAction($donate_id){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("donate_edition"));
   $donate=Donates::findFirst($donate_id);
   if($donate) $form=new Donate($donate);
   else{
    $this->flashSession->error($this->translate->_("no_donate_at_id",["id"=>$donate_id]));
    $this->response->redirect($this->url->get(["for"=>"sinor_donates"]));
   }
   if(!$this->request->getPost()) $this->view->form=$form;
   elseif($this->request->getPost()){
    if($form->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
     if($donate->save($this->request->getPost())){
      $this->flashSession->success($this->translate->_("donate_edition_success"));
      $this->response->redirect($this->url->get(["for"=>"sinor_donates"]));
     }
     else{
      foreach($donate->getMessages() as $message) $this->flashSession->error($message->getMessage());
      $this->view->form=$form;
     }
    }
    else{
     foreach($form->getMessages() as $message) $this->flashSession->error($message->getMessage());
     if(!count($form->getMessages()) && !$this->session->has("future_dates_validation")) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
     $this->view->form=$form;
    }
   }
  }
 }
 public function deleteDonateAction($donate_id){
  if($this->isLogged()){
   $donate=Donates::findFirst($donate_id);
   if($donate){
    $this->flashSession->success($this->translate->_("donate_deletion_success"));
    $donate->delete();
   }
   else $this->flashSession->error($this->translate->_("no_donate_at_id",["id"=>$donate_id]));
   $this->response->redirect($this->url->get(["for"=>"sinor_donates"]));
  }
 }
 public function deleteAction(){
  if($this->isLogged()){
   foreach($this->request->getPost("donates") as $donate_id) Donates::findFirst($donate_id)->delete();
   $this->flashSession->success($this->translate->_("donates_deletion_success",["donates_amount"=>count($this->request->getPost("donates"))]));
   $this->response->redirect($this->url->get(["for"=>"sinor_donates"]));
  }
 }
}
