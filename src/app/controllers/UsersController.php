<?php

namespace Sinor\Controller;

use Sinor\Model\Accounts;
use Sinor\Form\NewUser;

class UsersController extends BaseController{
 public function indexAction(){
  if($this->isLogged() && $this->session->access["username"]==="admin"){
   $this->tag->appendTitle(" - ".$this->translate->_("navigation_users"));
   $this->view->users=Accounts::find("username != 'admin'");
   $this->view->users_amount=count($this->view->users);
  }
  elseif($this->isLogged() && $this->session_>access["username"]!=="admin"){
   $this->flashSession->warning($this->translate->_("users_access_denied"));
   $this->response->redirect($this->url->get(["for"=>"sinor_index"]));
  }
 }
 public function newUserAction(){
  if($this->isLogged() && $this->session->access["username"]==="admin"){
   $this->tag->appendTitle(" - ".$this->translate->_("user_addition"));
   $new_user=new NewUser();
   if(!$this->request->getPost()) $this->view->form=$new_user;
   elseif($this->request->getPost()){
    if($new_user->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
     $account=new Accounts();
     if($account->save($this->request->getPost())){
      $this->flashSession->success($this->translate->_("user_addition_success",["username"=>$this->request->getPost("username")]));
      $this->response->redirect($this->url->get(["for"=>"sinor_users"]));
     }
     else{
      foreach($account->getMessages() as $message) $this->flashSession->error($message->getMessage());
      $this->view->form=$new_user;
     }
    }
    else{
     foreach($new_user->getMessages() as $message) $this->flashSession->error($message->getMessage());
     if(!count($new_user->getMessages())) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
     $this->view->form=$new_user;
    }
   }
  }
  elseif($this->isLogged() && $this->session->access["username"]!=="admin"){
   $this->flashSession->warning($this->translate->_("user_addition_access_denied"));
   $this->response->redirect($this->url->get(["for"=>"sinor_index"]));
  }
 }
 public function editUserAction($user_id){
  if($this->isLogged() && $this->session->access["username"]==="admin"){
   $this->tag->appendTitle(" - ".$this->translate->_("user_edition"));
   $account=Accounts::findFirst($user_id);
   if($account){
    $this->persistent->username=$account->username;
    $new_user=new NewUser($account);
   }
   else{
    $this->flashSession->error($this->translate->_("no_user_at_id",["id"=>$user_id]));
    $this->response->redirect($this->url->get(["for"=>"sinor_users"]));
   }
   if(!$this->request->getPost()) $this->view->form=$new_user;
   elseif($this->request->getPost()){
    if($new_user->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
     if($account->save($this->request->getPost(),["username","email"])){
      $this->flashSession->success($this->translate->_("user_edition_success",["username"=>$this->persistent->username]));
      $this->response->redirect($this->url->get(["for"=>"sinor_users"]));
     }
     else{
      foreach($account->getMessages() as $message) $this->flashSession->error($message->getMessage());
      $this->view->form=$new_user;
     }
    }
    else{
     foreach($new_user->getMessages() as $message) $this->flashSession->error($message->getMessage());
     if(!count($new_user->getMessages())) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
     $this->view->form=$new_user;
    }
   }
  }
  elseif($this->isLogged() && $this->session->access["username"]!=="admin"){
   $this->flashSession->warning($this->translate->_("user_edition_access_denied"));
   $this->response->redirect($this->url->get(["for"=>"sinor_index"]));
  }
 }
 public function deleteUserAction($user_id){
  if($this->isLogged() && $this->session->access["username"]==="admin"){
   $account=Accounts::findFirst($user_id);
   if($account){
    $this->flashSession->success($this->translate->_("user_deletion_success",["username"=>$account->username]));
    $account->delete();
   }
   else $this->flashSession->error($this->translate->_("no_user_at_id",["id"=>$user_id]));
   $this->response->redirect($this->url->get(["for"=>"sinor_users"]));
  }
  elseif($this->isLogged() && $this->session->access["username"]!=="admin"){
   $this->flashSession->warning($this->translate->_("user_deletion_access_denied"));
   $this->response->redirect($this->url->get(["for"=>"sinor_index"]));
  }
 }
 public function deleteAction(){
  if($this->isLogged() && $this->session->access["username"]==="admin"){
   foreach($this->request->getPost("users") as $user_id) Accounts::findFirst($user_id)->delete();
   $this->flashSession->success($this->translate->_("users_deletion_success",["users_amount"=>count($this->request->getPost("users"))]));
   $this->response->redirect($this->url->get(["for"=>"sinor_users"]));
  }
  elseif($this->isLogged() && $this->session->access["username"]!=="admin"){
   $this->flashSession->warning($this->translate->_("user_deletion_access_denied"));
   $this->response->redirect($this->url->get(["for"=>"sinor_index"]));
  }
 }
}
