<?php

namespace Sinor\Controller;

use Phalcon\Paginator\Adapter\Model as ModelPaginator;
use Sinor\Model\Sponsors;
use Sinor\Form\Sponsor;
use Sinor\Form\Search;

class SponsorsController extends BaseController{
 public function indexAction($page_number=1){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("navigation_sponsors"));
   if($this->request->getURI()==="/sponsors"){
    $this->session->remove("results");
    $this->session->remove("sort_type");
   }
   $sponsors=new ModelPaginator(["data"=>($this->session->results)? $this->session->results : Sponsors::find(["columns"=>"id, name, surname, nickname, email","order"=>$this->session->sort_type]),"limit"=>5,"page"=>$page_number]);
   $this->view->page=$sponsors->getPaginate();
   $this->view->form=new Search();
  }
 }
 public function sortAction($sort_type="nickname"){
  if($this->isLogged()){
   $this->session->set("sort_type",$sort_type);
   $this->dispatcher->forward(["controller"=>"Sinor\Controller\Sponsors","action"=>"index"]);
  }
 }
 public function searchAction(){
  if($this->isLogged()){
   $search=new Search();
   if($search->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
    $search_phrase=$this->request->getPost("phrase");
    $this->session->results=Sponsors::find(["name like '%$search_phrase%' or surname like '%$search_phrase%' or nickname like '%$search_phrase%'","columns"=>"id, name, surname, nickname, email","order"=>$this->session->sort_type]);
   }
   else{
    foreach($search->getMessages() as $message) $this->flashSession->error($message->getMessage());
    if(!count($search->getMessages())) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
   }
   $this->response->redirect($this->url->get(["for"=>"sinor_sponsorsPagination","page_number"=>1]));
  }
 }
 public function showAction($sponsor_id){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("view_sponsor"));
   $sponsor=Sponsors::findFirst($sponsor_id);
   if($sponsor){
    $this->view->sponsor=$sponsor;
    $this->view->donations=$sponsor->__get("Sinor\Model\Donates");
    foreach($this->view->donations as $donation) $this->view->donation_amount+=$donation->amount;
   }
   else{
    $this->flashSession->error($this->translate->_("no_sponsor_at_id",["id"=>$sponsor_id]));
    $this->response->redirect($this->url->get(["for"=>"sinor_sponsors"]));
   }
  }
 }
 public function newSponsorAction(){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("sponsor_addition"));
   $form=new Sponsor();
   if(!$this->request->getPost()) $this->view->form=$form;
   elseif($this->request->getPost()){
    $ns=new Sponsors();
    if($form->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
     if($ns->save($this->request->getPost())){
      $this->flashSession->success($this->translate->_("sponsor_addition_success",["nickname"=>$this->request->getPost("nickname")]));
      $this->response->redirect($this->url->get(["for"=>"sinor_sponsors"]));
     }
     else{
      foreach($ns->getMessages() as $message) $this->flashSession->error($message->getMessage());
      $this->view->form=$form;
     }
    }
    else{
     foreach($form->getMessages() as $message) $this->flashSession->error($message->getMessage());
     if(!count($form->getMessages())) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
     $this->view->form=$form;
    }
   }
  }
 }
 public function editSponsorAction($sponsor_id){
  if($this->isLogged()){
   $this->tag->appendTitle(" - ".$this->translate->_("sponsor_edition"));
   $sponsor=Sponsors::findFirst($sponsor_id);
   if($sponsor){
    $this->persistent->nickname=$sponsor->nickname;
    $form=new Sponsor($sponsor);
   }
   else{
    $this->flashSession->error($this->translate->_("no_sponsor_at_id",["id"=>$sponsor_id]));
    $this->response->redirect($this->url->get(["for"=>"sinor_sponsors"]));
   }
   if(!$this->request->getPost()) $this->view->form=$form;
   elseif($this->request->getPost()){
    if($form->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
     if($sponsor->save($this->request->getPost())){
      $this->flashSession->success($this->translate->_("sponsor_edition_success",["nickname"=>$this->persistent->nickname]));
      $this->response->redirect($this->url->get(["for"=>"sinor_sponsors"]));
     }
     else{
      foreach($sponsor->getMessages() as $message) $this->flashSession->error($message->getMessage());
      $this->view->form=$form;
     }
    }
    else{
     foreach($form->getMessages() as $message) $this->flashSession->error($message->getMessage());
     if(!count($form->getMessages())) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
     $this->view->form=$form;
    }
   }
  }
 }
 public function deleteSponsorAction($sponsor_id){
  if($this->isLogged()){
   $sponsor=Sponsors::findFirst($sponsor_id);
   if($sponsor){
    $this->flashSession->success($this->translate->_("sponsor_deletion_success",["nickname"=>$sponsor->nickname]));
    $sponsor->delete();
   }
   else $this->flashSession->error($this->translate->_("no_sponsor_at_id",["id"=>$sponsor_id]));
   $this->response->redirect($this->url->get(["for"=>"sinor_sponsors"]));
  }
 }
 public function deleteAction(){
  if($this->isLogged()){
   foreach($this->request->getPost("sponsors") as $sponsor_id) Sponsors::findFirst($sponsor_id)->delete();
   $this->flashSession->success($this->translate->_("sponsors_deletion_success",["sponsors_amount"=>count($this->request->getPost("sponsors"))]));
   $this->response->redirect($this->url->get(["for"=>"sinor_sponsors"]));
  }
 }
}
