<?php

namespace Sinor\Controller;

use Sinor\Form\Login;
use Sinor\Model\Accounts;
use Sinor\Model\Sponsors;
use Sinor\Model\Donates;

class IndexController extends BaseController{
 public function indexAction(){
  if($this->isLogged()){
   $this->view->sponsors=Sponsors::count();
   $this->view->donates=Donates::count();
   $this->view->donates_sum=($this->view->donates)? Donates::sum(["column"=>"amount"]) : 0;
  }
 }
 public function loginAction(){
  $login=new Login();
  $this->tag->appendTitle(" - ".$this->translate->_("sign_in"));
  if(!$this->request->getPost() && !$this->session->has("access")) $this->view->form=$login;
  elseif($this->request->getPost() && !$this->session->has("access")){
   if($login->isValid($this->request->getPost()) && $this->security->checkToken("csrf")){
    $account=Accounts::findFirstByUsername($this->request->getPost("username"));
    if($account && hash("sha512",$this->request->getPost("password"))===$account->password){
     $this->session->set("access",["username"=>$account->username,"password"=>$account->password]);
     $this->response->redirect($this->session->destination);
    }
    else{
     $this->flashSession->error($this->translate->_("bad_credentials_flash"));
     $this->view->form=$login;
    }
   }
   else{
    foreach($login->getMessages() as $message) $this->flashSession->error($message->getMessage());
    if(!count($login->getMessages())) $this->flashSession->error($this->translate->_("token_verification_failed_flash"));
    $this->view->form=$login;
   }
  }
  else return $this->response->redirect($this->url->get(["for"=>"sinor_index"]));
 }
 public function logoutAction(){
  $this->session->destroy(true);
  return $this->response->redirect($this->url->get(["for"=>"sinor_login"]));
 }
}
