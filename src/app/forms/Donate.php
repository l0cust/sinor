<?php

namespace Sinor\Form;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Message;
use Sinor\Model\Sponsors;

class Donate extends Form{
 public function initialize(){
  $id=(strpos($this->request->getURI(),"/new")===false)? "editDonate_" : "newDonate_";
  $donated=new Date("donated",["required"=>"required","placeholder"=>$this->translate->_("form_donated_placeholder"),"id"=>$id."donated"]);
  $donated->setLabel($this->translate->_("form_donated_label"));
  $donated->addValidator(new PresenceOf(["message"=>$this->translate->_("form_donated_validation_presence")]));
  $this->add($donated);
  $amount=new Text("amount",["required"=>"required","placeholder"=>$this->translate->_("form_amount_placeholder"),"id"=>$id."amount"]);
  $amount->setLabel($this->translate->_("form_amount_label"));
  $amount->addValidator(new Regex(["pattern"=>"/(\d+\.?\d*)/","message"=>$this->translate->_("form_amount_validation_regex")]));
  $this->add($amount);
  $sponsors[0]=$this->translate->_("select_sponsor");
  foreach(Sponsors::find() as $spn) $sponsors[$spn->id]=$spn->nickname;
  $sponsor=new Select("sponsor",$sponsors,["required"=>"required","placeholder"=>$this->translate->_("form_sponsor_placeholder"),"id"=>$id."sponsor"]);
  $sponsor->setLabel($this->translate->_("form_sponsor_label"));
  $sponsor->addValidator(new PresenceOf(["message"=>$this->translate->_("form_sponsor_validation_presence")]));
  $this->add($sponsor);
  $this->add(new Hidden("csrf"));
  $this->add(new Submit($this->translate->_("newDonate_form_submit"),["id"=>"newDonate_addDonate"]));
  $this->add(new Submit($this->translate->_("editDonate_form_submit"),["id"=>"editDonate_saveDonate"]));
  $this->setAction($this->request->getURI());
 }
 public function getCsrf(){
  return $this->security->getToken();
 }
 public function beforeValidation(){
  $value=$this->getElements()["donated"]->getValue();
  if(time()<strtotime($value)){
   $this->session->future_dates_validation=true;
   $this->flashSession->error($this->translate->_("form_donated_validation_future"));
   return false;
  }
  return true;
 }
}
