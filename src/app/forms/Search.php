<?php

namespace Sinor\Form;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;

class Search extends Form{
 public function initialize(){
  $phrase=new Text("phrase",["size"=>200,"required"=>"required","placeholder"=>$this->translate->_("form_phrase_placeholder"),"id"=>"search_phrase"]);
  $phrase->setLabel($this->translate->_("form_phrase_label"));
  $phrase->addValidator(new PresenceOf(["message"=>$this->translate->_("form_phrase_validation_presence")]));
  $this->add($phrase);
  $this->add(new Hidden("csrf"));
  $this->add(new Submit($this->translate->_("search_form_submit"),["id"=>"search_search"]));
  $this->setAction($this->url->get(["for"=>"sinor_searchSponsor"]));
 }
 public function getCsrf(){
  return $this->security->getToken();
 }
}
