<?php

namespace Sinor\Form;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\Regex;

class Sponsor extends Form{
 public function initialize(){
  $name=new Text("name",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_name_placeholder"),"id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_name" : "newSponsor_name"]);
  $name->setLabel($this->translate->_("form_name_label"));
  $this->add($name);
  $surname=new Text("surname",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_surname_placeholder"),"id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_surname" : "newSponsor_surname"]);
  $surname->setLabel($this->translate->_("form_surname_label"));
  $this->add($surname);
  $nickname=new Text("nickname",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_nickname_placeholder"),"required"=>"required","id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_nickname" : "newSponsor_nickname"]);
  $nickname->setLabel($this->translate->_("form_nickname_label"));
  $nickname->addValidators([
   new PresenceOf(["message"=>$this->translate->_("form_nickname_validation_presence")]),
   new Uniqueness(["model"=>"Sinor\Model\Sponsors","attribute"=>"nickname","message"=>$this->translate->_("form_nickname_validation_uniqueness")])
  ]);
  $this->add($nickname);
  $email=new Email("email",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_email_placeholder"),"id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_email" : "newSponsor_email"]);
  $email->setLabel($this->translate->_("form_email_label"));
  $this->add($email);
  $street=new Text("street",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_street_placeholder"),"id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_street" : "newSponsor_street"]);
  $street->setLabel($this->translate->_("form_street_label"));
  $street->addValidator(new Regex(["pattern"=>"/[a-zA-z0-9\/\sąĄćĆęĘłŁńŃóÓśŚźŹżŻ]*/","message"=>$this->translate->_("form_street_validation_regex")]));
  $this->add($street);
  $postcode=new Text("postcode",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_postcode_placeholder"),"id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_postcode" : "newSponsor_postcode"]);
  $postcode->setLabel($this->translate->_("form_postcode_label"));
  $postcode->addValidator(new Regex(["pattern"=>"/(\d{2}-\d{3})?/","message"=>$this->translate->_("form_postcode_validation_regex")]));
  $this->add($postcode);
  $city=new Text("city",["size"=>200,"maxlength"=>100,"placeholder"=>$this->translate->_("form_city_placeholder"),"id"=>(strpos($this->request->getURI(),"/new")===false)? "editSponsor_city" : "newSponsor_city"]);
  $city->setLabel($this->translate->_("form_city_label"));
  $this->add($city);
  $this->add(new Hidden("csrf"));
  $this->add(new Submit($this->translate->_("newSponsor_form_submit"),["id"=>"newSponsor_addSponsor"]));
  $this->add(new Submit($this->translate->_("editSponsor_form_submit"),["id"=>"editSponsor_saveSponsor"]));
  $this->setAction($this->request->getURI());
 }
 public function getCsrf(){
  return $this->security->getToken();
 }
}
