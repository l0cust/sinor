<?php

namespace Sinor\Form;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;

class Login extends Form{
 public function initialize(){
  $username=new Text("username",["maxlength"=>100,"placeholder"=>$this->translate->_("form_username_placeholder"),"size"=>200,"id"=>"login_username","required"=>"required"]);
  $username->setLabel($this->translate->_("form_username_label"));
  $username->addValidators([
   new PresenceOf(["message"=>$this->translate->_("form_username_validation_presence")]),
   new Regex(["pattern"=>"/[\w\.-]+/","message"=>$this->translate->_("form_username_validation_regex")])
  ]);
  $this->add($username);
  $password=new Password("password",["maxlength"=>100,"size"=>200,"placeholder"=>$this->translate->_("form_password_placeholder"),"id"=>"login_password","required"=>"required"]);
  $password->setLabel($this->translate->_("form_password_label"));
  $password->addValidators([
   new PresenceOf(["message"=>$this->translate->_("form_password_validation_presence")]),
   new StringLength(["min"=>8,"max"=>100,"messageMinimum"=>$this->translate->_("form_password_validation_minimum_length"),"messageMaximum"=>$this->translate->_("form_password_validation_maximum_length")])
  ]);
  $this->add($password);
  $this->add(new Hidden("csrf"));
  $this->add(new Submit($this->translate->_("login_form_submit"),["id"=>"login_signin"]));
  $this->setAction($this->request->getURI());
 }
 public function getCsrf(){
  return $this->security->getToken();
 }
}
