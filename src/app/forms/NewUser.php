<?php

namespace Sinor\Form;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email as EmailValidator;

class NewUser extends Form{
 public function initialize(){
  $username=new Text("username",["maxlength"=>100,"size"=>200,"placeholder"=>$this->translate->_("form_username_placeholder"),"required"=>"required","id"=>(strpos($this->request->getURi(),"/new")===false)? "editUser_username" : "newUser_username"]);
  $username->setLabel($this->translate->_("form_username_label"));
  $username->addValidators([
   new PresenceOf(["message"=>$this->translate->_("form_username_validation_presence")]),
   new Regex(["pattern"=>"/[\w\.-]+/","message"=>$this->translate->_("form_username_validation_regex")])
  ]);
  $this->add($username);
  if(strpos($this->request->getURI(),"/new")!==false){
   $password=new Password("password",["maxlength"=>100,"size"=>200,"placeholder"=>$this->translate->_("form_password_placeholder"),"required"=>"required","id"=>"newUser_password"]);
   $password->setLabel($this->translate->_("form_password_label"));
   $password->addValidators([
    new PresenceOf(["message"=>$this->translate->_("form_password_validation_presence")]),
    new StringLength(["min"=>8,"max"=>100,"messageMinimum"=>$this->translate->_("form_password_validation_minimum_length"),"messageMaximum"=>$this->translate->_("form_password_validation_maximum_length")])
   ]);
   $password->clear();
   $this->add($password);
  }
  $email=new Email("email",["maxlength"=>100,"size"=>200,"placeholder"=>$this->translate->_("form_email_placeholder"),"required"=>"required","id"=>(strpos($this->request->getURI(),"/new")===false)? "editUser_email" : "newUser_email"]);
  $email->setLabel($this->translate->_("form_email_label"));
  $email->addValidators([
   new PresenceOf(["message"=>$this->translate->_("form_email_validation_presence")]),
   new EmailValidator(["message"=>$this->translate->_("form_email_validation")])
  ]);
  $this->add($email);
  $this->add(new Hidden("csrf"));
  $this->add(new Submit($this->translate->_("newUser_form_submit"),["id"=>"newUser_addUser"]));
  $this->add(new Submit($this->translate->_("editUser_form_submit"),["id"=>"editUser_saveUserEdition"]));
  $this->setAction($this->request->getURI());
 }
 public function getCsrf(){
  return $this->security->getToken();
 }
}