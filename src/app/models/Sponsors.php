<?php

namespace Sinor\Model;

use Phalcon\Mvc\Model;

class Sponsors extends Model{
 public $id;
 public $name;
 public $surname;
 public $nickname;
 public $email;
 public $street;
 public $postcode;
 public $city;

 public function initialize(){
  $this->hasMany("id","Sinor\Model\Donates","sponsor");
 }
 public function columnMap(){
  return ["sponsor_id"=>"id","sponsor_name"=>"name","sponsor_surname"=>"surname","sponsor_nickname"=>"nickname","sponsor_email"=>"email","sponsor_street"=>"street","sponsor_postcode"=>"postcode","sponsor_city"=>"city"];
 }
 public function getSequenceName(){
  return "id";
 }
 public function getSource(){
  return "sponsors";
 }
}
