<?php

namespace Sinor\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\Email;

class Accounts extends Model{
 public $id;
 public $username;
 public $password;
 public $email;

 public function columnMap(){
  return ["account_id"=>"id","account_username"=>"username","account_password"=>"password","account_email"=>"email"];
 }
 public function getSequenceName(){
  return "id";
 }
 public function getSource(){
  return "accounts";
 }
 public function validation(){
  $this->validate(new Uniqueness(["field"=>"username","message"=>"Każdy użytkownik systemu musi mieć swoją unikalną nazwę."]));
  $this->validate(new Email(["field"=>"email"]));
  return ($this->validationHasFailed()!==true);
 }
 public function beforeSave(){
  if($this->di["request"]->getURI()==="/users/new") $this->password=hash("sha512",$this->password);
 }
}
