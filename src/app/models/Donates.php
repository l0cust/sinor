<?php

namespace Sinor\Model;

use Phalcon\Mvc\Model;
use Sinor\Validator\Date;

class Donates extends Model{
 public $id;
 public $donated;
 public $amount;
 public $sponsor;
 
 public function initialize(){
  $this->belongsTo("sponsor","Sinor\Model\Sponsors","id",["foreignKey"=>["message"=>$this->di["translate"]->_("sponsors_relation_validation")]]);
 }
 public function columnMap(){
  return ["donate_id"=>"id","donate_donated"=>"donated","donate_amount"=>"amount","donate_sponsor"=>"sponsor"];
 }
 public function getSequenceName(){
  return "id";
 }
 public function getSource(){
  return "donates";
 }
 public function validation(){
  $this->validate(new Date(["field"=>"donated"]));
  return ($this->validationHasFailed()!==true);
 }
}
